---
layout: post
title: Como customizar runner do GitLab
tags: gitlab docker
published: True
---

# Docker on MAC

Existem duas opções para instalação do Docer no Mac, Docker for Mac para OSX 10.10.3 rodando no Mac 2010 ou superior e o Docker Toolbox.
A diferença entre as duas é descrita [neste artigo][7], em resumo:

 - **Docker Toolbox**: Roda em cima de uma VirtualBox default com boot2docker instalado
 - **Docker for Mac**: Usa HyperKit (lightweight OS X virtualization), para rodar containers nativamente no Mac.

## Antes de instalar

Se o comando abaixo **não** retornar, você pode instalar o Docker for Mac.

{% highlight Bash %}
$ env | grep DOCKER
DOCKER_HOST=tcp://192.168.99.100:2376
DOCKER_MACHINE_NAME=default
DOCKER_TLS_VERIFY=1
DOCKER_CERT_PATH=/Users/victoriabialas/.docker/machine/machines/default
{% endhighlight %}

Se o comando acima **retornar** algum valor, você precisa remover DOCKER environment variables.

{% highlight Bash %}
unset DOCKER_TLS_VERIFY
unset DOCKER_CERT_PATH
unset DOCKER_MACHINE_NAME
unset DOCKER_HOST
{% endhighlight %}

## Instalação

Siga [este tutorial][6] para instalação.

# Customizar Gitlab runner docker

Gitlab permite você [rodar os builds em containers Docker][1]. Você pode por exemplo utilizar [DigitalOcean][5] (<== ganhe $10 de créditos usando este link) para [rodar seus builds][2].

1. Crie e publique em um container docker seu ambiente, existem vários [tutoriais na internet][8] que mostram como fazer isso
2. Configure no arquivo [.gitlab-ci.yml][0] seu container



[0]: http://docs.gitlab.com/ce/ci/yaml/README.html
[1]: http://docs.gitlab.com/ce/ci/docker/using_docker_images.html
[2]: https://about.gitlab.com/2016/04/19/how-to-set-up-gitlab-runner-on-digitalocean/
[3]: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner
[4]: https://about.gitlab.com/2016/04/19/gitlab-partners-with-digitalocean-to-make-continuous-integration-faster-safer-and-more-affordable/
[5]: www.digitalocean.com/?refcode=1bc10f2e0fc4
[6]: https://docs.docker.com/docker-for-mac/
[7]: https://docs.docker.com/docker-for-mac/docker-toolbox/
[8]: https://semaphoreci.com/community/tutorials/dockerizing-a-node-js-web-application
